from app.config import Config
from sqlalchemy import create_engine
from sqlalchemy.orm.session import sessionmaker
from app.models import Base, Channel, Program

engine = create_engine(Config.SQLALCHEMY_DATABASE_URI)

Base.metadata.create_all(engine)
Session = sessionmaker(bind=engine)
session = Session()
