import os

class Config:
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    DB_FILE = os.path.join(os.path.abspath(os.path.dirname(__file__)),'db', 'data.db')
    SQLALCHEMY_DATABASE_URI = "sqlite:///"+DB_FILE
    LOGGING_FILE = os.path.join(os.path.abspath(os.path.dirname(__file__)),'log', 'pytvguide.log')