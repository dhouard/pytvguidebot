import requests
import logging
from typing import List
from telebot import TeleBot, formatting
from datetime import datetime
from bs4 import BeautifulSoup
from app.config import Config

from app.models import Channel, Program, User
from app.repositories import ChannelRepository, UserRepository

logging.basicConfig(filename=Config.LOGGING_FILE)

def populate_channel_programs(channel: Channel, programs) -> Channel:
    for program_row in programs:
        time = program_row.find('div', 'channel-programs-time').find('a').text
        time = datetime.strptime(time, '%H:%M')
        name = program_row.find('div', 'channel-programs-title').find('a').text
        show_type = program_row.find('div', 'channel-programs-title').find('span').text
        description = program_row.find('div', 'channel-programs-field_program_description_value')
        if description is not None:
            description = description.find('a').text
        program = Program(name=name, type=show_type, description=description, time=time)
        channel.programs.append(program)

def scrape():
    resp = requests.get('https://www.tvguia.es')

    if resp.status_code != 200:
        raise Exception('Error al cargar tvguia.es')

    data = BeautifulSoup(resp.text, 'html.parser')
    table = data.find_all('div', 'tvlogo')
    repo = ChannelRepository()
    for row in table:
        link = row.find('a')
        channel = Channel(name=link['alt'], link=link['href'])
        if repo.exists(channel):
            channel = repo.find_by_name(channel=channel)
            channel.programs = []
            repo.save(channel)
        resp = requests.get('https://www.tvguia.es' + link['href'])
        if resp.status_code != 200:
            continue
        data = BeautifulSoup(resp.text, 'html.parser')
        programs = data.find_all('div', 'channel-row');
        try:
            populate_channel_programs(channel, programs)
        except Exception as e:
            logging.error(e.message)

        # First program of next day

        resp = requests.get('https://www.tvguia.es/' + link['href'].replace('/tv/', '/tv-manana/'))
        data = BeautifulSoup(resp.text, 'html.parser')
        program = data.find('div', 'channel-row');
        try:
            populate_channel_programs(channel, [program])
        except Exception as e:
            logging.error(e.message)


        repo.save(channel)


def bot():
    BOT_TOKEN = '6771180719:AAG7HfkUacEqJgKJ017msVUK74BYmFjhi3o'
    telebot = TeleBot(BOT_TOKEN)

    @telebot.message_handler(commands=['start', 'hello'])
    def send_welcome(message):
        telebot.reply_to(message, "Hola, ¿cómo andas?")

    @telebot.message_handler(commands=['channels', 'canales'])
    def send_channels(message):
        channels = _get_channels()
        telebot.reply_to(message, channels)

    @telebot.message_handler(commands=['myfavs', 'mifavs'])
    def get_favorite(message):
        myfavs = get_favorites(message.from_user.username)
        telebot.reply_to(message, myfavs, parse_mode="Markdown")

    @telebot.message_handler(func=lambda m: True)
    def parse(message):
        argument = _parse_command(message)
        telebot.reply_to(message, argument)

    telebot.infinity_polling()


def _get_channels():
    repo = ChannelRepository();
    result = repo.all()
    channels = ''
    for channel in result:
        channels += str(channel.id) + ' - ' + channel.name + '\n'
    return channels


def _parse_command(message):
    parts = message.text.split(" ")
    command = parts[0]
    parts = parts[1:]
    argument = ' '.join(parts)

    if command == '/now' or command == '/ahora':
        programs = _get_program(argument)
        return programs

    if command == '/next' or command == '/siguiente':
        programs = _get_program(argument, next=True)
        return programs

    if command == '/hora' or command == '/at' or command == '/alas':
        arguments = argument.split(" ")
        time = datetime.strptime(arguments[0], '%H:%M')
        channel = arguments[1:]
        channel = ' '.join(channel)
        programs = _get_program(channel, time=time)
        return programs
    if command == '/fav':
        user_repo = UserRepository()
        channel_repo = ChannelRepository()

        user = UserRepository.find_by_name(User(name=message.from_user.username))

        if user is None:
            user = User(name=message.from_user.username)
            user = user_repo.save(user)

        unfav = False

        if argument[-1] == '-':
            unfav = True
            argument = argument[:-2].strip()

        if argument.isnumeric():
            channel = channel_repo.get(argument)
        else:
            channel = channel_repo.find_by_name(Channel(name=argument))

        if channel:
            if unfav:
                for (index, fav) in enumerate(user.favorites):
                    if fav.name.upper() == channel.name.upper():
                        user.favorites.pop(index)
                        break
            else:
                found = [True for x in user.favorites if x.name == channel.name]
                if not found:
                    user.favorites.append(channel)
                    try:
                        user_repo.commit()
                        return 'Lista de favoritos modificada'
                    except:
                        return 'No se puedo añadir el favorito'
                else:
                    return 'El canal ya se encuentra en los favoritos'

    else:
        return 'Comando no reconocido'



def _get_program(channel: str, **kwargs) -> List:
    next = kwargs.get('next', False)
    time = kwargs.get('time', datetime.now())

    repo = ChannelRepository()
    if channel.isnumeric():
        channel = repo.get(channel)
    else:
        channel = Channel(name=channel)
        channel = repo.find_by_name(channel)
    if channel is not None:
        for (index, program) in enumerate(channel.programs):
            next_program = channel.programs[index + 1]
            if not next:
                if time.time() > program.time.time() and time.time() < next_program.time.time():
                    return  ('\n' + program.type + '\n' + program.time.strftime('%H:%M') + ' - ' +
                             next_program.time.strftime('%H:%M') + ' - ' + program.name +'\n\n' + program.description)

            if time.time() < program.time.time():
                try:
                    return ('\n' + program.type + '\n' + program.time.strftime('%H:%M') + ' - ' +
                            next_program.time.strftime('%H:%M') + ' - ' + program.name + '\n\n' + program.description)
                except IndexError:
                    return ('\n' + program.type + '\n' + program.time.strftime('%H:%M') + ' - ' +
                            program.name + '\n\n' + program.description)
                except BaseException as e:
                    logging.error(e.message)

        return channel

    return 'No reconozco ese canal'

def get_favorites(username: str) -> str:
    user_repo = UserRepository()
    channel_repo = ChannelRepository()

    user = user_repo.find_by_name(User(name=username))

    if user is None:
        return 'Usuario no reconocido'

    if len(user.favorites) == 0:
        return 'El usuario no tiene favoritos'

    my_channels = '\n\n'
    for channel in user.favorites:
        my_channels += formatting.format_text(formatting.mbold(channel.name)).replace('\\', '') + _get_program(channel.name) + '\n\n'

    return my_channels

def _get_channel_today_programs():
    pass


