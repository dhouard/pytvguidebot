from sqlalchemy import Column, Integer, String, DateTime, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class Program(Base):
    __tablename__ = 'programs'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    type = Column(String)
    description = Column(String)
    time = Column(DateTime)
    channel_id = Column(Integer, ForeignKey('channels.id'), nullable=False)

    channel = relationship('Channel', back_populates='programs')

class UserChannel(Base):
    __tablename__ = 'users_channels'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    channel_id = Column(Integer, ForeignKey('channels.id'))

class Channel(Base):
    __tablename__ = 'channels'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    link = Column(String)
    programs = relationship('Program', back_populates='channel', cascade='all, delete-orphan')

    users = relationship('User', secondary='users_channels', back_populates='favorites')

    def __str__(self):
        return 'name: ' + self.name + '\n' + 'link: ' + self.link

class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    favorites = relationship('Channel', secondary='users_channels', back_populates='users')
