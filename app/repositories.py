from typing import List
from app.database import session
from app.models import Channel, Program, User
from sqlalchemy import select, func
from sqlalchemy.exc import NoResultFound


class ChannelRepository:
    @classmethod
    def all(cls) -> List:
        return session.query(Channel).all()
    @classmethod
    def save(cls, channel: Channel) -> Channel:
        session.add(channel)
        return session.commit()

    @classmethod
    def find_by_name(cls, channel: Channel) -> Channel:
        try:
            return session.query(Channel).filter(func.upper(Channel.name) == func.upper(channel.name)).one()
        except NoResultFound:
            return None

    @classmethod
    def get(cls, id) -> Channel:
        return session.query(Channel).get(id)

    @classmethod
    def exists(cls, channel: Channel) -> bool:
        channels: int = session.query(Channel).filter(Channel.name == channel.name).count()
        return channels != 0

    @classmethod
    def commit(cls):
        session.commit()

    @classmethod
    def program_now(cls, channel: str) -> List :
       stmt = select(Program).filter_by(name=channel)
       result = session.execute(stmt)
       programs = []
       for program in result:
           programs.append(program)

       return programs

class UserRepository():
    @classmethod
    def all(cls) -> List:
        return session.query(User).all()
    @classmethod
    def save(cls, user: User) -> User:
        session.add(user)
        return session.commit()

    @classmethod
    def find_by_name(cls, user: User) -> User:
        try:
            return session.query(User).filter(func.upper(user.name) == func.upper(User.name)).one()
        except NoResultFound:
            return None
        except Exception as e:
            return e.message

    @classmethod
    def get(cls, id) -> User:
        return session.query(User).get(id)

    @classmethod
    def exists(cls, user: User) -> bool:
        users: int = session.query(User).filter(User.name == user.name).count()
        return users != 0

    @classmethod
    def commit(cls):
        session.commit()